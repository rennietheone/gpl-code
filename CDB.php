



<?php

class CDB {

	public function getTableFields($table_name) {
		$connection = $this->getConnectionObject();
		$result     = $connection->query("DESCRIBE $table_name");

		if ($result) {
			$rowa = array();

			while ($row = $result->fetch_assoc()) {
				$rowa[] = $row;

			}
			$fields = array();
			foreach ($rowa as $row) {

				$fields[] = $row['Field'];

			}
			return $fields;

		} else {

			return "<br>No Results<br>";

		}
	}

	public function getMySQLDataArray($query) {

		$connection = $this->getConnectionObject();
		$result     = $connection->query($query);

		if ($result) {

			$rowa = array();

			while ($row = $result->fetch_assoc()) {
				$rowa[] = $row;

			}
			return $rowa;

		} else {

			return "<br>No Results<br>";

		}
	}

	public function createTable($table_name) {

		$conn = $this->getConnectionObject();
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		// sql to create table
		$sql = "CREATE TABLE $table_name (
			id INT(9) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			test_field TEXT,
			date_created TIMESTAMP
		)";

		if ($conn->query($sql) === TRUE) {
			echo "Table <b>$table_name</b> created successfully<br><br>";
		} else {
			echo "Error creating table: " . $conn->error;
		}

		$conn->close();

	}

	public function dropTable($table_name) {

		$conn = $this->getConnectionObject();

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		// sql to create table
		$sql = "DROP TABLE $table_name";

		if ($conn->query($sql) === TRUE) {
			echo "Table <b>$table_name</b> is deleted successfully<br><br>";
		} else {
			echo "Error deleting table: " . $conn->error . "<br><br>";
		}

		$conn->close();

	}

	public function getConnectionObject() {

		$servername = "localhost";
		$username   = "root";
		$password   = "";
		$dbname     = "rennietheone";

		$connection = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($connection->connect_error) {
			die("Connection failed: " . $connection->connect_error);
		}
		return $connection;

	}

	public function getAllUserIds($table_name) {

		$connection = $this->getConnectionObject();

		$sql    = "SELECT DISTINCT bitrix_user_id FROM $table_name";
		$result = $connection->query($sql);
		if ($result) {
			$rows = array();
			while ($row = $result->fetch_assoc()) {
				$rows[] = $row;

			}

		} else {

			echo "No data";
		}

	}

	public function getAllUserIdsLastDate($table_name) {

		$connection = $this->getConnectionObject();

		$sql    = "SELECT bitrix_user_id, max(date_created) FROM $table_name GROUP BY bitrix_user_id";
		$result = $connection->query($sql);
		if ($result) {

			while ($row = $result->fetch_assoc()) {
				$rows[] = $row;

			}

			$last_viewers = "<ul>";
			foreach ($rows as $row) {

				$last_viewers .= "<li>User ID: " . $row['bitrix_user_id'] . " Последний визит: " . $row['max(date_created)'] . "<span class='watch_all' data-user-id='" . $row['bitrix_user_id'] . "'>Смотреть все</span></li>";

			}
			$last_viewers .= "</ul>";
			echo $last_viewers;

		} else {

			echo "No data";
		}

	}

	public function getDataArray($table_name, $user_id) {

		$connection = $this->getConnectionObject();

		$sql = "SELECT * FROM $table_name WHERE bitrix_user_id=$user_id";

		$connection->query("SET NAMES 'utf8'");
		$result = $connection->query($sql);

		$output       = "<table class='table'>";
		$column_names = array();
		if ($result) {

			$rowa = array();

			while ($row = $result->fetch_assoc()) {
				$rowa[] = $row;

			}
			$column_names = array_keys($rowa[0]);
			echo "<pre>";
			echo "</pre>";
			$output .= "<tr>";
			foreach ($column_names as $name) {

				$output .= "<th>" . $name . "</th>";
			}
			$output .= "</tr>";

			foreach ($rowa as $key => $row_data) {

				$output .= "<tr>";
				foreach ($row_data as $key => $value) {

					$output .= "<td>$value</td>";

				}

				$output .= "</tr>";
			}

			$output .= "</table>";
			echo $output;
		} else {
			echo "0 results";
		}

		$connection->close();

	}

	public function showTable($table_name) {

		$connection = $this->getConnectionObject();

		$sql = "SELECT * FROM $table_name";

		$connection->query("SET NAMES 'utf8'");
		$result = $connection->query($sql);

		$output       = "<table class='table' id='full-table'>";
		$column_names = array();

		if ($result->num_rows) {

			$rowa = array();

			while ($row = $result->fetch_assoc()) {
				$rowa[] = $row;

			}
			$column_names = array_keys($rowa[0]);
			echo "<pre>";

			echo "</pre>";
			$output .= "<tr>";
			foreach ($column_names as $name) {

				$output .= "<th>" . $name . "</th>";
			}
			$output .= "</tr>";

			foreach ($rowa as $key => $row_data) {

				$output .= "<tr>";
				foreach ($row_data as $key => $value) {

					if ($key == 'yandex_password' || $key == 'yandex_token') {
						$value = "Secured";
					}

					$output .= "<td>$value</td>";

				}

				$output .= "</tr>";

			}

			$output .= "</table>";
			echo $output;
		} else {
			echo "0 results";
		}

		$connection->close();

	}

	public function showTableByUserID($table_name, $user_id) {

		$connection = $this->getConnectionObject();

		$data = array();

		$data['bitix_user_id'] = $user_id;

		$sql = "SELECT * FROM $table_name WHERE bitrix_user_id=$user_id";

		$connection->query("SET NAMES 'utf8'");
		$result = $connection->query($sql);

		$output       = "<table class='table'>";
		$column_names = array();
		if ($result) {
			$rowa = array();
			while ($row = $result->fetch_assoc()) {
				$rowa[] = $row;

			}
			$column_names = array_keys($rowa[0]);
			echo "<pre>";
			echo "</pre>";
			$output .= "<tr>";
			foreach ($column_names as $name) {

				$output .= "<th>" . $name . "</th>";
			}
			$output .= "</tr>";

			foreach ($rowa as $key => $row_data) {
				$output .= "<tr>";
				foreach ($row_data as $key => $value) {

					$output .= "<td>$value</td>";

				}

				$output .= "</tr>";
			}

			$output .= "</table>";
			$data['output'] = $output;
			echo $output;
		} else {
			echo "0 results";
		}

		$connection->close();

	}

	public function saveStandardDataToDB($data, $table_name) {

		$conn = $this->getConnectionObject();

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		$columns = implode(", ", array_keys($data));
		$asd     = array_values($data);

		$values = "";
		foreach ($asd as $value) {

			$values .= "'$value', ";

		}
		$values = substr($values, 0, -2);

		$sql = "INSERT INTO $table_name ($columns) VALUES ($values)";
		$conn->query("SET NAMES 'utf8'");
		if ($conn->query($sql) === TRUE) {
			echo "New record created successfully";
		} else {
			echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();

	}

	public function createTableByTableName($table_name) {

		$all_tables = array(

			'test',

		);

		if (!$table_name) {
			die("<br>Не указано название таблицы");
		}

		if (!in_array($table_name, $all_tables)) {
			die("<br>Табица <b>$table_name</b> не найдена");
		}

		$conn = $this->getConnectionObject();

		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}

		switch ($table_name) {

			case 'test':
				$sql = "CREATE TABLE $table_name (
					id INT(2) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
					test_name VARCHAR(128),
					description TEXT,
					date_created TIMESTAMP
				)";
				break;

			default:

				echo "<br>Шаблон таблицы не найден<br>";

				break;
		}

		$collation = "CHARACTER SET utf8 collate utf8_unicode_ci";

		$sql = $sql . $collation;

		if ($conn->query($sql) === TRUE) {
			echo "<br><br>Table <b>$table_name</b> created successfully<br><br>";
		} else {
			echo "<br><br>Error creating table: " . $conn->error . "<br><br>";
		}

		$conn->close();
	}

	public function tableExists($table_name) {
		$conn  = $this->getConnectionObject();
		$query = "SELECT 1 FROM $table_name LIMIT 1";

		if ($conn->query($query)) {
			return true;
		} else {
			return false;
		}

	}

	public function getStyle($style_name) {
		$conn = $this->getConnectionObject();

		$query  = "SELECT style_content FROM styles where style_name = '$style_name'";
		$result = $conn->query($query);

		//print_r($result);exit;
		if ($result) {

			while ($a = $result->fetch_assoc()) {

				$style = $a['style_content'];
				//$result = $a;

			}
			echo $style;
		} else {

			return 0;
		}
	}

	public function addDataForm($table_name) {
		$test = true;

		if (!$_POST) {

			$test = false;

		}

		foreach ($_POST as $item) {

			if ($test) {

			}

			if ($item == "") {

				$test = false;

			}
		}
		if ($test) {

			if ($this->tableExists($table_name)) {

				$this->saveStandardDataToDB($_POST, $table_name);

			} else {
				$this->createTableByTableName($table_name);
				$this->saveStandardDataToDB($_POST, $table_name);
			}

			echo "<br>Данные добавлены в таблицу <b>$table_name</b><br>";

		} else if ($_POST) {
			echo "<h5>Заполните все поля формы</h5>";
		}
	}

	public function editDataForm($table_name, $id) {
		$test = true;

		if (!$_POST) {

			$test = false;

		}

		foreach ($_POST as $item) {

			if ($test) {

			}

			if ($item == "") {

				$test = false;

			}
		}
		if ($test) {

			if ($this->tableExists($table_name)) {

				$this->editData($table_name, $id, $_POST);

			} else {
				$this->createTableByTableName($table_name);
				$this->saveStandardDataToDB($_POST, $table_name);
			}

		} else if ($_POST) {
			echo "<h5>Заполните все поля формы</h5>";
		}
	}

	public function showForm($form_name) {
		if (!$form_name) {
			die("Укажите название формы");
		}

		switch ($form_name) {

			case 'test':
				echo

				'<form action="" method="POST">
					<h3>Новый тест</h3>

					<div class="form-div">
						<label for="">Название теста</label>
						<input type="text" name="test_name">
					</div>

					<div class="form-div">
						<label for="">Описание теста</label>
						<textarea name="description"></textarea>
					<div class="form-div">


					<input type="submit" value="Добавить тест">'
				;
				break;

			default:
				echo "<br>Неизвестная форма";
				break;
		}
	}

	public function editData($table_name, $row_id, $fields) {
		$set = "";

		foreach ($fields as $key => $value) {

			$set .= "$key='$value',";

		}

		$set = rtrim($set, ",");

		$update_query = "UPDATE $table_name SET $set WHERE id='$row_id'";

		$connection = $this->getConnectionObject();

		$connection->query($update_query);
	}

	public function getField($table_name, $id, $field) {

		$query = "SELECT $field FROM $table_name WHERE id = '$id' LIMIT 1";
		$data  = $this->getMySQLDataArray($query);

		if (is_array($data)) {
			return $data[0][$field];
		}

	}

	public function addStyle($stylesheet_name) {
		echo "<link rel='stylesheet' type='text/css' href='css/$stylesheet_name.css'>";
	}

	public function getPage($page) {

		if (!$this->tableExists($page)) {

			foreach (scandir("includes") as $include_page) {

				if ($page . ".htm" == $include_page) {
					include "includes/$include_page";
				}

			}
			return 0;
		}

		$this->showTable($page);
		$this->showForm($page);
		$this->addDataForm($page);

	}

}